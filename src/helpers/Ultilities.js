const getRandomNumber = () => {
  return Number(
    Math.floor(Math.random() * (9999 - 1 + 1) + 1).toFixed(1)
  ).toLocaleString();
};

const isEmptyVal = (val) => {
  return !!(
    !val ||
    val.toString().trim() === "" ||
    typeof val === undefined ||
    val.length === 0
  );
};

export { getRandomNumber, isEmptyVal };
