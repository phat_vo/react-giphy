import { getRandomNumber } from "helpers/Ultilities";

export default class GifsTrending {
  constructor(data) {
    return data.map((item) => {
      return {
        ...item,
        imgSrc: item.images?.original?.url,
        isVerified: !!item?.user?.is_verified,
        comment: getRandomNumber(),
        like: getRandomNumber(),
        view: getRandomNumber(),
        ...(item?.user
          ? {
              avartar: item?.user?.avatar_url,
              owner: item?.user?.username,
            }
          : {}),
      };
    });
  }
}
