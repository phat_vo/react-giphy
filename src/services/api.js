const API_ENDPOINT = process.env.REACT_APP_API_SERVICE;
const API_KEY = process.env.REACT_APP_SERVICE_KEY;

export const getGifsTrending = (limit, offset) =>
  `${API_ENDPOINT}/gifs/trending?api_key=${API_KEY}&limit=${limit}&offset=${offset}`;
