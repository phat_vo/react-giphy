import axios from "axios";
import { getGifsTrending } from "./api";

import { GifsTrending } from "models";
import { LIMIT_RECORDS } from "constants/index";

export const fetchGifsTrending = async (offset, callback) => {
  await axios
    .get(getGifsTrending(LIMIT_RECORDS, offset))
    .then((response) => {
      if (response.status === 200) {
        const { data, pagination } = response?.data;
        const formaData = new GifsTrending(data);
        callback(true, formaData, pagination);
      }
    })
    .catch((error) => {
      callback(error);
    });
};
