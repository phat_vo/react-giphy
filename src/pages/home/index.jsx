import React, { useEffect, useState } from "react";

// import libs
import { Container, Row } from "reactstrap";
import { toast } from "react-toastify";
import InfiniteScroll from "react-infinite-scroll-component";

// import actions and constants
import { fetchGifsTrending } from "services";
import { isEmptyVal } from "helpers/Ultilities";
import { LIMIT_RECORDS } from "constants/index";

// import comon components
import FullPicture from "components/full-picture/FullPicture";
import CardView from "components/card-view/CardView";
import { Loader, GrowLoader } from "components/loader";

// import style
import "./style.scss";

const HomePage = (props) => {
  const [trendingData, setTrendingData] = useState({
    data: [],
    page: 0,
    totalPage: 0,
  });

  const [isLoading, setIsLoading] = useState(true);

  const [modal, setModal] = useState({
    isOpen: false,
    srcImage: "",
    imageTitle: "",
  });

  const onFetchData = () => {
    const currentPage = trendingData.page + 1;
    fetchGifsTrending(
      currentPage * LIMIT_RECORDS,
      (status, res, pagination) => {
        if (status) {
          const pages = pagination
            ? Math.floor((pagination.total_count - 1) / LIMIT_RECORDS + 1)
            : 0;
          setTrendingData({
            page: currentPage,
            totalPage: pages,
            data: [...trendingData.data, ...res],
          });
        } else {
          toast.error(res);
        }
        setIsLoading(false);
      }
    );
  };

  useEffect(() => {
    onFetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onShowPicture = (link, title) => {
    setModal({ isOpen: true, srcImage: link, imageTitle: title });
  };

  const onClosePicture = () => {
    setModal({ isOpen: false, srcImage: "", imageTitle: "" });
  };

  return (
    <Container className="themed-container mt-5" data-testid="list">
      {modal.isOpen && <FullPicture data={modal} onClose={onClosePicture} />}
      {!isEmptyVal(trendingData.data) ? (
        <InfiniteScroll
          dataLength={trendingData.data.length}
          next={() => onFetchData()}
          hasMore={trendingData.page < trendingData.totalPage}
          loader={<GrowLoader />}
        >
          <Row xs="2" sm="2" md="3" lg="4">
            {trendingData.data.map((item, key) => (
              <CardView key={key} onShowPicture={onShowPicture} data={item} />
            ))}
          </Row>
        </InfiniteScroll>
      ) : isLoading ? (
        <Loader />
      ) : (
        <h1 className="text-center">Opps, no data conection found!</h1>
      )}
    </Container>
  );
};

export default HomePage;
