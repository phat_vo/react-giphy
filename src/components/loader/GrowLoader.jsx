import React from "react";
import PropTypes from "prop-types";
import { Spinner } from "reactstrap";

const GrowLoader = ({ isRefresh }) => {
  return (
    <div className="d-flex w-100 m-auto pt-3 pb-5">
      <div className="w-content m-auto">
        {isRefresh ? (
          <Spinner color="info" />
        ) : (
          <>
            <Spinner type="grow" color="success" />
            <Spinner type="grow" color="success" />
            <Spinner type="grow" color="success" />
          </>
        )}
      </div>
    </div>
  );
};
GrowLoader.propTypes = {
  isRefresh: PropTypes.bool,
};
export default GrowLoader;
