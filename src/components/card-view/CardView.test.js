import React from "react";
import renderer from "react-test-renderer";
import CardView from "./CardView";

test("Render CardView", () => {
  const dataTest = {
    data: {
      type: "gif",
      id: "OnKlrHN7Decko",
      url: "https://giphy.com/gifs/OnKlrHN7Decko",
      slug: "OnKlrHN7Decko",
      bitly_gif_url: "http://gph.is/296BUNI",
      bitly_url: "http://gph.is/296BUNI",
      embed_url: "https://giphy.com/embed/OnKlrHN7Decko",
      username: "",
      source: "http://imgur.com/gallery/jcuGCWW",
      title: "Church Saturday GIF",
      rating: "g",
      content_url: "",
      source_tld: "imgur.com",
      source_post_url: "http://imgur.com/gallery/jcuGCWW",
      is_sticker: 0,
      import_datetime: "2016-06-30 19:29:43",
      trending_datetime: "2021-05-01 10:15:05",
      images: {
        original: {
          height: "153",
          width: "200",
          size: "411500",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.gif&ct=g",
          mp4_size: "616309",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.mp4&ct=g",
          webp_size: "150294",
          webp: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.webp?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.webp&ct=g",
          frames: "42",
          hash: "50fc35907eb90098eb3c651aa43e7646",
        },
        downsized: {
          height: "153",
          width: "200",
          size: "411500",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.gif&ct=g",
        },
        downsized_large: {
          height: "153",
          width: "200",
          size: "411500",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.gif&ct=g",
        },
        downsized_medium: {
          height: "153",
          width: "200",
          size: "411500",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.gif&ct=g",
        },
        downsized_small: {
          height: "152",
          width: "200",
          mp4_size: "152117",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy-downsized-small.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy-downsized-small.mp4&ct=g",
        },
        downsized_still: {
          height: "153",
          width: "200",
          size: "411500",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy_s.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy_s.gif&ct=g",
        },
        fixed_height: {
          height: "200",
          width: "261",
          size: "758091",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/200.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200.gif&ct=g",
          mp4_size: "159866",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/200.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200.mp4&ct=g",
          webp_size: "195922",
          webp: "https://media3.giphy.com/media/OnKlrHN7Decko/200.webp?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200.webp&ct=g",
        },
        fixed_height_downsampled: {
          height: "200",
          width: "261",
          size: "113842",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/200_d.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200_d.gif&ct=g",
          webp_size: "62556",
          webp: "https://media3.giphy.com/media/OnKlrHN7Decko/200_d.webp?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200_d.webp&ct=g",
        },
        fixed_height_small: {
          height: "100",
          width: "131",
          size: "221528",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/100.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=100.gif&ct=g",
          mp4_size: "46193",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/100.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=100.mp4&ct=g",
          webp_size: "58758",
          webp: "https://media3.giphy.com/media/OnKlrHN7Decko/100.webp?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=100.webp&ct=g",
        },
        fixed_height_small_still: {
          height: "100",
          width: "131",
          size: "6275",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/100_s.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=100_s.gif&ct=g",
        },
        fixed_height_still: {
          height: "200",
          width: "261",
          size: "21740",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/200_s.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200_s.gif&ct=g",
        },
        fixed_width: {
          height: "153",
          width: "200",
          size: "334128",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/200w.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200w.gif&ct=g",
          mp4_size: "94837",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/200w.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200w.mp4&ct=g",
          webp_size: "146186",
          webp: "https://media3.giphy.com/media/OnKlrHN7Decko/200w.webp?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200w.webp&ct=g",
        },
        fixed_width_downsampled: {
          height: "153",
          width: "200",
          size: "48342",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/200w_d.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200w_d.gif&ct=g",
          webp_size: "39722",
          webp: "https://media3.giphy.com/media/OnKlrHN7Decko/200w_d.webp?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200w_d.webp&ct=g",
        },
        fixed_width_small: {
          height: "77",
          width: "100",
          size: "149074",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/100w.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=100w.gif&ct=g",
          mp4_size: "31281",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/100w.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=100w.mp4&ct=g",
          webp_size: "41504",
          webp: "https://media3.giphy.com/media/OnKlrHN7Decko/100w.webp?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=100w.webp&ct=g",
        },
        fixed_width_small_still: {
          height: "77",
          width: "100",
          size: "4470",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/100w_s.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=100w_s.gif&ct=g",
        },
        fixed_width_still: {
          height: "153",
          width: "200",
          size: "8321",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/200w_s.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=200w_s.gif&ct=g",
        },
        looping: {
          mp4_size: "3832539",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy-loop.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy-loop.mp4&ct=g",
        },
        original_still: {
          height: "153",
          width: "200",
          size: "11472",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy_s.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy_s.gif&ct=g",
        },
        original_mp4: {
          height: "366",
          width: "480",
          mp4_size: "616309",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.mp4&ct=g",
        },
        preview: {
          height: "114",
          width: "150",
          mp4_size: "25765",
          mp4: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy-preview.mp4?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy-preview.mp4&ct=g",
        },
        preview_gif: {
          height: "57",
          width: "75",
          size: "49040",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy-preview.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy-preview.gif&ct=g",
        },
        preview_webp: {
          height: "130",
          width: "170",
          size: "38702",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/giphy-preview.webp?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy-preview.webp&ct=g",
        },
        "480w_still": {
          height: "367",
          width: "480",
          size: "411500",
          url: "https://media3.giphy.com/media/OnKlrHN7Decko/480w_s.jpg?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=480w_s.jpg&ct=g",
        },
      },
      analytics_response_payload:
        "e=Z2lmX2lkPU9uS2xySE43RGVja28mZXZlbnRfdHlwZT1HSUZfVFJFTkRJTkcmY2lkPTFhMWJkMWE4dnE0cXgwdzRhOWEzZjUxcDN4cXM3M2hqcXJtOThsa2x1eXJqODg3OCZjdD1naWY",
      analytics: {
        onload: {
          url: "https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPU9uS2xySE43RGVja28mZXZlbnRfdHlwZT1HSUZfVFJFTkRJTkcmY2lkPTFhMWJkMWE4dnE0cXgwdzRhOWEzZjUxcDN4cXM3M2hqcXJtOThsa2x1eXJqODg3OCZjdD1naWY&action_type=SEEN",
        },
        onclick: {
          url: "https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPU9uS2xySE43RGVja28mZXZlbnRfdHlwZT1HSUZfVFJFTkRJTkcmY2lkPTFhMWJkMWE4dnE0cXgwdzRhOWEzZjUxcDN4cXM3M2hqcXJtOThsa2x1eXJqODg3OCZjdD1naWY&action_type=CLICK",
        },
        onsent: {
          url: "https://giphy-analytics.giphy.com/v2/pingback_simple?analytics_response_payload=e%3DZ2lmX2lkPU9uS2xySE43RGVja28mZXZlbnRfdHlwZT1HSUZfVFJFTkRJTkcmY2lkPTFhMWJkMWE4dnE0cXgwdzRhOWEzZjUxcDN4cXM3M2hqcXJtOThsa2x1eXJqODg3OCZjdD1naWY&action_type=SENT",
        },
      },
      imgSrc:
        "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.gif&ct=g",
      isVerified: false,
      comment: "1,760",
      like: "5,934",
      view: "4,232",
    },
    onShowPicture: jest.fn(),
  };

  const component = renderer.create(<CardView {...dataTest} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
