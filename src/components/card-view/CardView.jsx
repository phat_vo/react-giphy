import React from "react";
import PropTypes from "prop-types";
import { Card, CardImg, CardBody, Col } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import LazyLoad from "react-lazyload";
import {
  faPaperclip,
  faEye,
  faComment,
  faHeart,
} from "@fortawesome/free-solid-svg-icons";
import { GrowLoader } from "components/loader";
import "./CardView.scss";

const CardView = ({ data, onShowPicture }) => {
  const { imgSrc, title, isVerified, comment, like, view, owner, avartar } =
    data;

  const RatingValues = [
    { icon: faEye, value: view },
    { icon: faComment, value: comment },
    { icon: faHeart, value: like },
  ];

  const generalRatingValue = (icon, value, key) => {
    return (
      <span key={key}>
        <FontAwesomeIcon icon={icon} color="grey" className="me-1" /> {value}
      </span>
    );
  };

  return (
    <Col className="mb-4">
      <Card>
        <LazyLoad
          once
          placeholder={<GrowLoader isRefresh={true} />}
          debounce={500}
        >
          <CardImg
            top
            width="100%"
            src={imgSrc}
            alt={title}
            className="card-image ps-3 pt-3 pe-3"
            onClick={() => onShowPicture(imgSrc, title)}
          />
        </LazyLoad>

        <CardBody className="card-body">
          <span>
            {isVerified && <FontAwesomeIcon icon={faPaperclip} color="grey" />}
          </span>

          {RatingValues.map((item, key) => {
            return generalRatingValue(item.icon, item.value, key);
          })}
        </CardBody>
      </Card>

      {(avartar || owner) && (
        <div className="d-flex mt-2 ms-3">
          {avartar && (
            <img
              src={avartar}
              alt={owner ? owner : "anonymous"}
              className="rounded-circle ml-2"
            />
          )}
          {owner && <p className="article-name p-0 mt-0 mb-0 ms-2">{owner}</p>}
        </div>
      )}
    </Col>
  );
};

CardView.propTypes = {
  data: PropTypes.shape({
    imgSrc: PropTypes.string,
    title: PropTypes.string,
    isVerified: PropTypes.bool,
    comment: PropTypes.string,
    like: PropTypes.string,
    view: PropTypes.string,
    owner: PropTypes.string,
    avartar: PropTypes.string,
  }),
  onShowPicture: PropTypes.func,
};

export default CardView;
