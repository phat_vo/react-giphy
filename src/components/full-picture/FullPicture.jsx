import React from "react";
import PropTypes from "prop-types";
import { ModalBody, Modal, ModalHeader } from "reactstrap";
import "./FullPicture.scss";

const FullPicture = ({ data, onClose }) => {
  const { isOpen, srcImage, imageTitle } = data;
  return (
    <Modal
      isOpen={isOpen}
      toggle={onClose}
      centered={true}
      fade={true}
      className="modal-fullscreen"
    >
      <ModalHeader toggle={onClose}></ModalHeader>
      <ModalBody>
        <img src={srcImage} alt={imageTitle} className="full-picture-view" />
      </ModalBody>
    </Modal>
  );
};

FullPicture.propTypes = {
  data: PropTypes.shape({
    isOpen: PropTypes.bool.isRequired,
    fsrcImage: PropTypes.string,
    imageTitle: PropTypes.string,
  }).isRequired,
  onClose: PropTypes.func.isRequired,
};

FullPicture.defaultProps = {
  imageTitle: "Trending image",
};

export default FullPicture;
