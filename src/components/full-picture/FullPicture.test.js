import React from "react";
import renderer from "react-test-renderer";
import FullPicture from "./FullPicture";

test("Render FullPicture", () => {
  const dataTest = {
    data: {
      isOpen:false,
      srcImage:
        "https://media3.giphy.com/media/OnKlrHN7Decko/giphy.gif?cid=1a1bd1a8vq4qx0w4a9a3f51p3xqs73hjqrm98lkluyrj8878&rid=giphy.gif&ct=g",
      imageTitle: "test-image",
    },
    onClose: jest.fn(),
  };

  const component = renderer.create(<FullPicture {...dataTest} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
